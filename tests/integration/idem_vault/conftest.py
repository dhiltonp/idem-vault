import uuid
from typing import Any
from typing import Dict

import pytest


@pytest.fixture(scope="function")
async def vault_kv_v2_secret(hub, ctx, version) -> Dict[str, Any]:
    """
    Create and cleanup an vault_kv_v2_secret for a module that needs it
    """
    if version == "v1":
        pytest.skip()
    secret_name = "idem-test-get-kv-v2-secret-" + str(uuid.uuid4())
    path = "secret/idem-test-get-kv-v2-secret-1"
    data = {"my-secret": "my-secret-value"}

    ret = await hub.states.vault.secrets.kv_v2.secret.present(
        ctx=ctx, name=secret_name, path=path, data=data
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    yield ret.get("new_state")

    ret = await hub.states.vault.secrets.kv_v2.secret.absent(
        ctx=ctx, name=secret_name, path=path
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")


@pytest.fixture(scope="function")
async def vault_kv_v1_secret(hub, ctx, version) -> Dict[str, Any]:
    """
    Create and cleanup an vault_kv_v1_secret for a module that needs it
    """
    if version == "v2":
        pytest.skip()
    secret_name = "idem-test-get-kv-v1-secret-" + str(uuid.uuid4())
    path = "secret/idem-test-get-kv-v1-secret-1"
    data = {"my-secret": "my-secret-value"}

    ret = await hub.states.vault.secrets.kv_v1.secret.present(
        ctx=ctx, name=secret_name, path=path, data=data
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    yield ret.get("new_state")

    ret = await hub.states.vault.secrets.kv_v1.secret.absent(
        ctx=ctx, name=secret_name, path=path
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
