import pytest
from pytest_idem.runner import run_sls
from pytest_idem.runner import run_yaml_block

SECRET_PATH = "secret/idem_test_sls_source"


@pytest.mark.dependency(name="present")
def test_present(hub, ctx, esm_cache, acct_data, version):
    """
    Create a brand new secret for testing the vault sls source
    """
    PRESENT_STATE = f"""
    test_resource:
      vault.secrets.kv_{version}.secret.present:
        - path: {SECRET_PATH}
        - data:
            id: secret
            token: 1234
            username: user
    """
    # Run the state defined in the yaml block
    ret = run_yaml_block(PRESENT_STATE, managed_state=esm_cache, acct_data=acct_data)
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])


@pytest.mark.dependency(depends=["present"])
def test_cache(hub, ctx, tests_dir):
    ret = run_sls(
        ["test"],
        acct_data={"profiles": {"vault": {"test_development_idem_vault": ctx.acct}}},
        params=[SECRET_PATH],
        param_sources=[
            f"vault://test_development_idem_vault@{SECRET_PATH}",
        ],
        hard_fail_on_collect=True,
        sls_sources=[f"file://{tests_dir}/sls"],
        render="jinja|yaml",
    )
    assert ret
    assert len(ret) == 1, "Expecting 1 state"
    single_state = next(iter(ret.values()))
    assert single_state["result"], single_state["comment"]
    assert single_state["new_state"] == {
        "id": "secret",
        "token": 1234,
        "user": "user",
        "username": "user",
    }


@pytest.mark.dependency(depends=["present"])
def test_absent(esm_cache, acct_data, version):
    """
    Clean up the secret created for this test
    """
    ABSENT_STATE = f"""
    test_resource:
      vault.secrets.kv_{version}.secret.absent
    """

    # Run the state defined in the yaml block
    ret = run_yaml_block(ABSENT_STATE, managed_state=esm_cache, acct_data=acct_data)
    resource_ret = next(iter(ret.values()))
    assert resource_ret["result"], "\n".join(resource_ret["comment"])
    assert resource_ret["new_state"] is None
