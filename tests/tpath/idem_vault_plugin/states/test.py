def present(
    hub,
    ctx,
    name: str,
    old_state=None,
    changes=None,
    new_state=None,
    result=True,
    force_save=None,
    **kwargs,
):
    """
    Return the previous old_state, if it's not specified in sls, and the given new_state.
    Raise an error on fail

    This is a copy of present from idem's tests/tpath/*/states/test.py file.
    """
    if old_state is None:
        old_state = ctx.get("old_state")
    ret = {
        "name": name,
        "old_state": old_state,
        "new_state": new_state,
        "changes": changes,
        "result": result,
        "comment": None,
    }
    if force_save is not None:
        ret["force_save"] = force_save
    return ret
